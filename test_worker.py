import sys, os, time, random
import kimiworker
from minio import Minio

def mockedJobHandler(
    log: kimiworker.kimilogger.KimiLogger,
    job: kimiworker.types.WorkerJob,
    job_id: str,
    local_file_path: str,
    minio: Minio,
    publish: kimiworker.types.PublishMethod,
):
    log.info("Mocked job handler started")
    log.info(f"Record ID: {job.get('recordId')}")
    log.info(f"File Path: {local_file_path}")
    time.sleep(random.randint(5, 15))

    publish("stringResult", "application/pdf", success=True, confidence=100)

# TODO: Make more tests

def test_worker():
    worker = kimiworker.Worker(job_handler=mockedJobHandler, queue_name="worker-foo")
    assert worker.job_handler == mockedJobHandler
    assert worker.job_listener.amqp_host == "localhost"
    assert worker.job_listener.amqp_port == "1234"
    assert worker.job_listener.amqp_username == "amqp-user"
    assert worker.job_listener.amqp_password == "amqp-pass"
    assert worker.job_listener.exchange_prefix == "foo-exchange"
    assert worker.minio_host == "localhost"
    assert worker.minio_port == "1234"
    assert worker.minio_access_key == "minio-access-key"
    assert worker.minio_secret_key == "minio-secret-key"
