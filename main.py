import sys, os
import kimiworker
from tesserocr import PyTessBaseAPI
from PIL import Image
from pathdict import PathDict

numberOfConcurrentJobs = int(os.getenv("CONCURRENT_JOBS", 1))


def jobHandler(log, job, jobId, localFilePath, minio, publish):
    recordId = job.get("recordId")
    attributes = PathDict(job.get("attributes"))

    # TODO: bestJobId is optional, so a check needs to be added
    bestJobId = attributes["elementSpec.bestJobId"]
    values = attributes["elementSpec.values"]
    bestValue = next((x for x in values if x["jobId"] == bestJobId), None)

    if bestValue is None:
        publish(
            "contentResult",
            [],
            success=False,
            confidence=0,
        )
        return

    contentResult = bestValue["contentResult"][0]
    elementClassification = contentResult.get("classification")
    elementBoundingBox = contentResult.get("bbox")
    recordIdParts = recordId.split("|||")
    elementIndex = int(recordIdParts[3])

    log.info("Opening image")
    image = Image.open(localFilePath)

    imageWidth, imageHeight = image.size

    api = PyTessBaseAPI(lang="nld")
    api.SetImage(image)

    log.info("Getting text from image")
    delta = image.size[0] / 100  # Delta to improve OCR results within bounding box

    boxLeft = elementBoundingBox.get("left", 0) * imageWidth
    boxTop = elementBoundingBox.get("top", 0) * imageHeight
    boxWidth = (
        elementBoundingBox.get("right", 0) - elementBoundingBox.get("left", 0)
    ) * imageWidth
    boxHeight = (
        elementBoundingBox.get("bottom", 0) - elementBoundingBox.get("top", 0)
    ) * imageHeight

    api.SetRectangle(
        max(boxLeft - delta, 0),
        max(boxTop - delta, 0),
        min(boxWidth + 2 * delta, imageWidth),
        min(boxHeight + 2 * delta, imageHeight),
    )

    text = api.GetUTF8Text()
    confidence = api.MeanTextConf()

    if text != "":
        log.info("Job done")
        publish(
            "contentResult",
            [
                {
                    "classification": elementClassification,
                    "bbox": elementBoundingBox,
                    "attributes": {
                        "text": text,
                        "confidence": confidence,
                        "elementIndex": elementIndex,
                        "origin": f"Tesseract {api.Version()}",
                    },
                }
            ],
            success=True,
            confidence=confidence,
        )
    else:
        log.warning("Empty text from image")
        publish(
            "contentResult",
            [
                {
                    "classification": elementClassification,
                    "bbox": elementBoundingBox,
                    "attributes": {
                        "text": "",
                        "confidence": confidence,
                        "elementIndex": elementIndex,
                        "origin": f"Tesseract {api.Version()}",
                    },
                }
            ],
            success=True,
            confidence=confidence,
        )

    log.info("Closing image")
    image.close()


if __name__ == "__main__":
    try:
        worker = kimiworker.Worker(jobHandler, "worker-extract-element-text", True)
        worker.start(numberOfConcurrentJobs)

    except KeyboardInterrupt:
        print("\n")
        print("Got interruption signal. Exiting...\n")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
